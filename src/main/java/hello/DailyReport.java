package hello;

import java.util.List;

public class DailyReport {
    public String getMydailyReport(List<ProductHistory> myhistorylist) {
        double totalkcal = 0;
        for (ProductHistory myproduct : myhistorylist) {
            totalkcal += myproduct.getProduct().getKcal() * myproduct.getCantity();
        }
        if (totalkcal <= 1000) {
            return "Cazul 1 kcal= " + totalkcal;
        } else if (totalkcal <= 2000 && totalkcal > 1000) {
            return "Cazul 2 kcal= " + totalkcal;
        } else {
            return "cazul 3 kcal= " + totalkcal;
        }
    }
}
