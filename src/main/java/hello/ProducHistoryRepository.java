package hello;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

import java.util.Date;
import java.util.List;

public interface ProducHistoryRepository extends CrudRepository<ProductHistory, Long> {

    List<ProductHistory> findAllByDataLessThanEqualAndDataGreaterThanEqual(Date from, Date to);

    List<ProductHistory> findAll();

}
