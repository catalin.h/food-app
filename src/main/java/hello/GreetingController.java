package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.*;


import java.text.ParseException;
import java.util.Date;


@Controller

@RequestMapping("/greeting")

public class GreetingController {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProducHistoryRepository producthistoryreporsitory;

    @GetMapping()
    public String greeting(@RequestParam(value = "name", required = false, defaultValue = "World") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }

    @PostMapping(value = "/add")
    public String add(@RequestParam(value = "food", defaultValue = "name") String food,
                      @RequestParam(value = "kcal", defaultValue = "0") double kcal,
                      @RequestParam(value = "quantity", defaultValue = "0") double quantity,
                      Model model) {
        if (productRepository.findByName(food) != null && quantity != 0) {
            ProductHistory productHistory = new ProductHistory();
            productHistory.setCantity(quantity);
            productHistory.setData(new Date());
            productHistory.setProduct(productRepository.findByName(food));
            producthistoryreporsitory.save(productHistory);
            model.addAttribute("added", true);
        } else if (productRepository.findByName(food) == null && kcal != 0 && quantity != 0) {
            Product product = new Product();
            product.setName(food);
            product.setKcal(kcal);
            productRepository.save(product);
            ProductHistory productHistory = new ProductHistory();
            productHistory.setCantity(quantity);
            productHistory.setData(new Date());
            productHistory.setProduct(product);
            producthistoryreporsitory.save(productHistory);
            model.addAttribute("added", true);
        }

        return "greeting";
    }

    @PostMapping(value = "/daily")
    public String add(Model model) {
        DailyReport dailyreport = new DailyReport();
        Date myday = new Date();
        model.addAttribute("gettotal", dailyreport.getMydailyReport(producthistoryreporsitory.findAll()));
        model.addAttribute("dailyreport", true);
        return "greeting";
    }

    @PostMapping(value = "/monthreport")
    public String report(@RequestParam(value = "frommonth") String month, @RequestParam(value = "fromday") String day,
                         @RequestParam(value = "tomonth") String tomonth, @RequestParam(value = "today") String today,
                         Model model) throws ParseException {
        Htmlp_text htmltable = new Htmlp_text();
        QuerryData querryDatafrom = new QuerryData();

        QuerryData querryDatato = new QuerryData();
        System.out.println(producthistoryreporsitory.findAllByDataLessThanEqualAndDataGreaterThanEqual(
                querryDatato.datequerry(tomonth, today), querryDatafrom.datequerry(month, day)));
        System.out.println(querryDatafrom.datequerry(month, day) + "   " + querryDatato.datequerry(tomonth, today));
        model.addAttribute("getname", htmltable.stringHtmltable(
                producthistoryreporsitory.findAllByDataLessThanEqualAndDataGreaterThanEqual(
                        querryDatato.datequerry(tomonth, today), querryDatafrom.datequerry(month, day))));
        model.addAttribute("producthistory", producthistoryreporsitory.findAll());
        return "monthreport";
    }


}











