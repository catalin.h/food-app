package hello;

import ch.qos.logback.classic.pattern.DateConverter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class QuerryData {

    public Date datequerry(String a, String b) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String format = new String("2018-" + a + "-" + b + " 00:00:00");
        Date date = new Date();
        date = dateFormat.parse(format);
        return date;
    }
}
