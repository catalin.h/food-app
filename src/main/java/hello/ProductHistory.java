package hello;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.Date;

@Entity
public class ProductHistory {
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;
    private double quantity;
    private Date data;
    @ManyToOne
    private Product product;


    public ProductHistory() {
        this.id = id;
        this.quantity = quantity;
        this.data = data;
        this.product = product;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductHistory)) return false;

        ProductHistory that = (ProductHistory) o;

        if (Double.compare(that.getCantity(), getCantity()) != 0) return false;
        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) return false;
        if (getData() != null ? !getData().equals(that.getData()) : that.getData() != null) return false;
        return getProduct() != null ? getProduct().equals(that.getProduct()) : that.getProduct() == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getId() != null ? getId().hashCode() : 0;
        temp = Double.doubleToLongBits(getCantity());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (getData() != null ? getData().hashCode() : 0);
        result = 31 * result + (getProduct() != null ? getProduct().hashCode() : 0);
        return result;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getCantity() {
        return quantity;
    }

    public void setCantity(double cantity) {
        this.quantity = cantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "ProductHistory{" +
                "id=" + id +
                ", quantity=" + quantity +
                ", data=" + data +
                ", product=" + product +
                '}';
    }
}
